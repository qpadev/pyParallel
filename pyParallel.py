#!/bin/python2.7
import urllib2
import socks
import socket
import os
import argparse
import time
import logging
from urlparse import urlparse

class urlDownloader():

	def __init__(self,url,byteRange,interface):
		self.hostname = urlparse(url).hostname
		self.path = urlparse(url).path
		self.byteRange = byteRange
		self.interface = interface
		self.firstRun = True

	def connect(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		try:
			self.sock.setsockopt(1, 25, self.interface)
		except:
			logging.error("Errore impostazione interfaccia di download, controlla i nomi delle interfaccia o prova sudo ./pyParallel.py")
			quit()

		try:
			self.sock.connect((self.hostname, 80))
		except:
			logging.error("Errore connessione socket")
			quit()

		self.httpRequest = "GET " + self.path + " HTTP/1.1\nHost: " + self.hostname + "\nRange: bytes=" + self.byteRange + "\nUser-Agent: curl/7.49.1\nAccept: */*\nConnection: Keep-Alive\n\n"

		self.sock.sendall(self.httpRequest)

	def read(self,bufferSize):
		try:
			recived = self.sock.recv(bufferSize)
			if self.firstRun:
				self.firstRun = False
				return recived[recived.find("\r\n\r\n")+4:]
			else:
				return recived
		except:
			logging.error("Errore ricezione socket")
			return None

	def close(self):
		try:
			self.sock.close()
		except:
			logging.error("Errore chiusura socket")

def downloadUrlFile(url,AUTO_DL,outputFile,interface):

	if not outputFile:
		file_name = url.split('/')[-1]
	else:
		file_name = outputFile

	localfile = os.getcwd() + "/" + file_name
	bytesRangeStart = 0
	file_size_dl = 0
	answer="."

	try:
		if os.path.isfile(localfile):
			if not AUTO_DL:
				while not answer in ["Y","N"]:
					answer = raw_input("File "+ file_name + " gia' presente, riprendere il download? Y/N ").upper()

			if answer == "Y" or AUTO_DL:
				file_size_dl = os.path.getsize(localfile)
				bytesRangeStart = file_size_dl
				f = open(file_name, 'ab')
			else:
				f = open(file_name, 'wb')

		else:
			f = open(file_name, 'wb')
	except:
		logging.exception("Errore apertura file")
		quit()

	file_size = getUrlFileSize(url,"B")

	byteRange = str(bytesRangeStart)+"-"+str(file_size)

	if file_size_dl >= file_size:
		print("File gia' scaricato")
	else:
		page = urlDownloader(url,byteRange,interface)
		page.connect()
		block_size = 65536#8192

		print("Downloading: %s Bytes: %s" % (file_name, file_size))

		while True:
			time1 = time.time()
			buffer = page.read(block_size)
			if not buffer:
				break
			file_size_dl += len(buffer)
			f.write(buffer)
			time2 = time.time()
			status = r"%4d/%d MB [%3.2f%%]  %5d KiB/s" % (file_size_dl/1000000, file_size/1000000,file_size_dl * 100. / file_size, len(buffer)/(1000*(time2-time1)))
			print(status)

	f.close()

def getUrlFileSize(url,format):
	div = {'MB': 1000000, 'KB': 1000, 'B': 1}

	try:
		u = urllib2.urlopen(url)
		meta = u.info()
		return int(meta.getheaders("Content-Length")[0])/div[format]
	except:
		raise

def main():
	loggingLevels = {1:logging.WARNING,2:logging.INFO,3:logging.DEBUG}

	parser = argparse.ArgumentParser(description="Script in python per scaricare file da internet sfruttando piu' connessioni internet contemporaneamente.")
	parser.add_argument("-l","--link", action="store",required=True,dest="link", help="Link da scaricare",default="")
	parser.add_argument("-i","--interfaces", action="store",dest="interfaces", help="Interfaccie per il download. Es: 'eth1,wlan1'",default="")
	parser.add_argument("-v","--verbose", action="store",default=30, type=int, dest="verboseLevel", help="Output avanzato. Warning 1, Info 2, Debug 3. Default: 1")

	arg = parser.parse_args()

	try:
		logging.basicConfig(format='%(levelname)s:%(message)s', level=loggingLevels[arg.verboseLevel])
	except:
		logging.basicConfig(format='%(levelname)s:%(message)s', level=loggingLevels[1])
		logging.error("Valore verbose non valido")

	interfaces = arg.interfaces.split(",")

	downloadUrlFile(arg.link,False,"",interfaces[0])


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print('\nUscita')
		quit()